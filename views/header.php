<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8"/>
        <link rel="stylesheet" href="/css/default.css">   </head>

    <body >
        <header>

            <div id="title">
            Framework MVC PHP. Curso 2017/2018</div>

            <nav>
            <ul>
                <li><a href="/">Inicio</a></li>
                <li><a href="/author">Autores</a></li>
                <li><a href="/book">Libros</a></li>
                <li><a href="/user">Usuarios</a></li>

                <?php if(isset($_SESSION['book'])): ?>
                <li>Ultimo libro:
                    <a href="/book/forget">
                    <?php echo $_SESSION['book']->title ?>
                    </a>
                </li>
                <?php endif?>
            </ul>
            </nav>
            <nav style="float: right;">
            <ul >
                <?php if (isset($_SESSION['user'])): ?>
                    <li style="color: #fff">
                        <?php
                        echo $_SESSION['user']->name;
                        ?>
                    </li>
                    <li><a href="/login/logout">Salir</a></li>

                <?php else: ?>
                    <li style="color: #fff">
                        <?php
                        echo 'Invitado';
                        ?>
                    </li>
                    <li><a href="/login">Login</a></li>
                <?php endif ?>





            </ul>
            </nav>
        </header>

